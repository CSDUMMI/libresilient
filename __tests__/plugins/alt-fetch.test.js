const makeServiceWorkerEnv = require('service-worker-mock');

global.fetch = require('node-fetch');
jest.mock('node-fetch')

/*
 * we need a Promise.any() polyfill
 * so here it is
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/any
 * 
 * TODO: remove once Promise.any() is implemented broadly
 */
if (typeof Promise.any === 'undefined') {
    Promise.any = async (promises) => {
        // Promise.all() is the polar opposite of Promise.any()
        // in that it returns as soon as there is a first rejection
        // but without it, it returns an array of resolved results
        return Promise.all(
            promises.map(p => {
                return new Promise((resolve, reject) =>
                    // swap reject and resolve, so that we can use Promise.all()
                    // and get the result we need
                    Promise.resolve(p).then(reject, resolve)
                );
            })
        // now, swap errors and values back
        ).then(
            err => Promise.reject(err),
            val => Promise.resolve(val)
        );
    };
}

describe("plugin: alt-fetch", () => {
  
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    global.LibResilientPluginConstructors = new Map()
    init = {
        name: 'alt-fetch',
        endpoints: [
            'https://alt.resilient.is/test.json',
            'https://error.resilientis/test.json',
            'https://timeout.resilientis/test.json'
        ]}
    LR = {
        log: (component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        }
    }
  })
  
  test("it should register in LibResilientPluginConstructors", () => {
    require("../../plugins/alt-fetch.js");
    expect(LibResilientPluginConstructors.get('alt-fetch')().name).toEqual('alt-fetch');
  });
  
  test("it should fail with bad config", () => {
    init = {
        name: 'alt-fetch',
        endpoints: "this is incorrect"
    }
    require("../../plugins/alt-fetch.js")
    expect.assertions(1)
    expect(()=>{
        LibResilientPluginConstructors.get('alt-fetch')(LR, init)
    }).toThrow(Error);
  });
  
  test("it should fetch the content, trying all configured endpoints (if fewer or equal to concurrency setting)", async () => {
    require("../../plugins/alt-fetch.js");
    
    global.fetch.mockImplementation((url, init) => {
          const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: url
                            });
          return Promise.resolve(response);
        });
    
    const response = await LibResilientPluginConstructors.get('alt-fetch')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(fetch).toHaveBeenCalledTimes(3);
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  })
  
  test("it should fetch the content using, trying <concurrency> random endpoints out of all configured (if more than concurrency setting)", async () => {
    
    init = {
        name: 'alt-fetch',
        endpoints: [
            'https://alt.resilient.is/test.json',
            'https://error.resilientis/test.json',
            'https://timeout.resilientis/test.json',
            'https://alt2.resilient.is/test.json',
            'https://alt3.resilient.is/test.json',
            'https://alt4.resilient.is/test.json'
        ]}
    
    require("../../plugins/alt-fetch.js");
    
    global.fetch.mockImplementation((url, init) => {
          const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: url
                            });
          return Promise.resolve(response);
        });
    
    const response = await LibResilientPluginConstructors.get('alt-fetch')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(fetch).toHaveBeenCalledTimes(3);
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  })
  
  test("it should pass the Request() init data to fetch() for all used endpoints", async () => {
    
    init = {
        name: 'alt-fetch',
        endpoints: [
            'https://alt.resilient.is/test.json',
            'https://error.resilientis/test.json',
            'https://timeout.resilientis/test.json',
            'https://alt2.resilient.is/test.json',
            'https://alt3.resilient.is/test.json',
            'https://alt4.resilient.is/test.json'
        ]}
    
    require("../../plugins/alt-fetch.js");
    
    global.fetch.mockImplementation((url, init) => {
          const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: url
                            });
          return Promise.resolve(response);
        });
    
    var initTest = {
        method: "GET",
        headers: new Headers({"x-stub": "STUB"}),
        mode: "mode-stub",
        credentials: "credentials-stub",
        cache: "cache-stub",
        referrer: "referrer-stub",
        // these are not implemented by service-worker-mock
        // https://github.com/zackargyle/service-workers/blob/master/packages/service-worker-mock/models/Request.js#L20
        redirect: undefined,
        integrity: undefined,
        cache: undefined
    }
    
    const response = await LibResilientPluginConstructors.get('alt-fetch')(LR, init).fetch('https://resilient.is/test.json', initTest);
    
    expect(fetch).toHaveBeenCalledTimes(3);
    expect(fetch).toHaveBeenNthCalledWith(1, expect.stringContaining('/test.json'), initTest);
    expect(fetch).toHaveBeenNthCalledWith(2, expect.stringContaining('/test.json'), initTest);
    expect(fetch).toHaveBeenNthCalledWith(3, expect.stringContaining('/test.json'), initTest);
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  })
  
  test("it should set the LibResilient headers", async () => {
    require("../../plugins/alt-fetch.js");

    const response = await LibResilientPluginConstructors.get('alt-fetch')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(fetch).toHaveBeenCalledTimes(3);
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
    expect(response.headers.has('X-LibResilient-Method')).toEqual(true)
    expect(response.headers.get('X-LibResilient-Method')).toEqual('alt-fetch')
    expect(response.headers.has('X-LibResilient-Etag')).toEqual(true)
    expect(response.headers.get('X-LibResilient-ETag')).toEqual('TestingETagHeader')
  });
  
  test("it should set the LibResilient ETag basd on Last-Modified header (if ETag is not available in the original response)", async () => {
    require("../../plugins/alt-fetch.js");
    
    global.fetch.mockImplementation((url, init) => {
          const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'Last-Modified': 'TestingLastModifiedHeader'
                              },
                              url: url
                            });
          return Promise.resolve(response);
        });

    const response = await LibResilientPluginConstructors.get('alt-fetch')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(fetch).toHaveBeenCalledTimes(3);
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
    expect(response.headers.has('X-LibResilient-Method')).toEqual(true)
    expect(response.headers.get('X-LibResilient-Method')).toEqual('alt-fetch')
    expect(response.headers.has('X-LibResilient-Etag')).toEqual(true)
    expect(response.headers.get('X-LibResilient-ETag')).toEqual('TestingLastModifiedHeader')
  });
  
  test("it should throw an error when HTTP status is >= 400", async () => {
    
    global.fetch.mockImplementation((url, init) => {
            const response = new Response(
                                new Blob(
                                  ["Not Found"],
                                  {type: "text/plain"}
                                ),
                                {
                                  status: 404,
                                  statusText: "Not Found",
                                  url: url
                                });
            return Promise.resolve(response);
          });
    
    require("../../plugins/alt-fetch.js");

    expect.assertions(1)
    expect(LibResilientPluginConstructors.get('alt-fetch')(LR, init).fetch('https://resilient.is/test.json')).rejects.toThrow(Error)
  });
  
});
