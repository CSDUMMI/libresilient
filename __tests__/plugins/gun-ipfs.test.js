const makeServiceWorkerEnv = require('service-worker-mock');

describe("plugin: gun-ipfs", () => {
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    init = {
        name: 'gun-ipfs',
        gunPubkey: 'stub'
    }
    global.LibResilientPluginConstructors = new Map()
    LR = {
        log: jest.fn((component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        })
    }
    global.Ipfs = {
        create: ()=>{
            return Promise.resolve({
                get: ()=>{
                    return {
                        next: ()=>{
                            sourceUsed = true
                            return Promise.resolve({
                                value: {
                                    path: 'some-ipfs-looking-address',
                                    content: {
                                        next: ()=>{
                                            sourceUsed = !sourceUsed
                                            return Promise.resolve({
                                                done: sourceUsed,
                                                value: Uint8Array.from(
                                                            Array
                                                                .from('{test: "success"}')
                                                                .map(
                                                                    letter => letter.charCodeAt(0)
                                                                )
                                                        )
                                            })
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
    }
    self.Ipfs = global.Ipfs
    self.gunUser = jest.fn(()=>{
      return {
        get: () => {
          return {
            get: ()=>{
              return {
                once: (arg)=>{ arg(undefined) }
              }
            }
          }
        }
      }
    })
    global.Gun = jest.fn((nodes)=>{
      return {
        user: self.gunUser
      }
    })
  })
  
  test("it should register in LibResilientPlugins", () => {
    require("../../plugins/gun-ipfs.js");
    expect(LibResilientPluginConstructors.get('gun-ipfs')(LR, init).name).toEqual('gun-ipfs');
  });
  
  test("IPFS setup should be initiated", async ()=>{
    self.importScripts = jest.fn()
    require("../../plugins/gun-ipfs.js");
    try {
        await LibResilientPluginConstructors.get('gun-ipfs')(LR, init).fetch('/test.json')
    } catch {}
    expect(self.importScripts).toHaveBeenNthCalledWith(1, './lib/ipfs.js')
  })
  
  test("Gun setup should be initiated", async ()=>{
    self.importScripts = jest.fn()
    require("../../plugins/gun-ipfs.js");
    try {
        await LibResilientPluginConstructors.get('gun-ipfs')(LR, init).fetch('/test.json')
    } catch {}
    expect(self.importScripts).toHaveBeenNthCalledWith(2, "./lib/gun.js", "./lib/sea.js", "./lib/webrtc.js")
  })
  
  test("fetching should error out for unpublished content", async ()=>{
    require("../../plugins/gun-ipfs.js");
    
    expect.assertions(1)
    try {
      await LibResilientPluginConstructors.get('gun-ipfs')(LR, init).fetch(self.location.origin + '/test.json')
    } catch(e) {
      expect(e).toEqual(new Error('IPFS address is undefined for: /test.json'))
    }
  })
  
  test("fetching a path ending in <path>/ should instead fetch <path>/index.html", async ()=>{
    require("../../plugins/gun-ipfs.js");
    
    expect.assertions(1)
    try {
      await LibResilientPluginConstructors.get('gun-ipfs')(LR, init).fetch(self.location.origin + '/test/')
    } catch(e) {
      expect(e).toEqual(new Error('IPFS address is undefined for: /test/index.html'))
    }
  })
  
  test("content types should be guessed correctly when fetching", async ()=>{
    require("../../plugins/gun-ipfs.js");
    var gunipfsPlugin = LibResilientPluginConstructors.get('gun-ipfs')(LR, init)
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test/')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : text/html")
    LR.log.mockClear()
    
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test.htm')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : text/html")
    LR.log.mockClear()
    
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test.css')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : text/css")
    LR.log.mockClear()
    
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test.js')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : text/javascript")
    LR.log.mockClear()
    
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test.json')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : application/json")
    LR.log.mockClear()
    
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test.svg')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : image/svg+xml")
    LR.log.mockClear()
    
    try {
      await gunipfsPlugin.fetch(self.location.origin + '/test.ico')
    } catch(e) {}
    expect(LR.log).toHaveBeenCalledWith('gun-ipfs', "   +-- guessed contentType : image/x-icon")
  })
  
  test("fetching should work (stub!)", async ()=>{
    self.gunUser = jest.fn(()=>{
      return {
        get: () => {
          return {
            get: ()=>{
              return {
                once: (arg)=>{ arg('some-ipfs-looking-address') }
              }
            }
          }
        }
      }
    })
    require("../../plugins/gun-ipfs.js");
    
    //await self.Ipfs.create()
    let response = await LibResilientPluginConstructors.get('gun-ipfs')(LR, init).fetch(self.location.origin + '/test.json')
    expect(response.body.type).toEqual('application/json')
    expect(String.fromCharCode.apply(null, response.body.parts[0])).toEqual('{test: "success"}')
  })
  
  test("publishContent should error out if passed anything else than string or array of string", async ()=>{
    require("../../plugins/gun-ipfs.js");
    var gunipfsPlugin = LibResilientPluginConstructors.get('gun-ipfs')(LR, init)
    expect(()=>{
      gunipfsPlugin.publish({
        url: self.location.origin + '/test.json'
      })
    }).toThrow('Handling a Response: not implemented yet')
    expect(()=>{
      gunipfsPlugin.publish(true)
    }).toThrow('Only accepts: string, Array of string, Response.')
    expect(()=>{
      gunipfsPlugin.publish([true, 5])
    }).toThrow('Only accepts: string, Array of string, Response.')
  })
});
