const makeServiceWorkerEnv = require('service-worker-mock');

describe("plugin: basic-integrity", () => {
  
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    
    global.LibResilientPluginConstructors = new Map()
    LR = {
        log: (component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        }
    }
    
    global.resolvingFetch = jest.fn((url, init)=>{
                                return Promise.resolve(
                                    new Response(
                                        new Blob(
                                            [JSON.stringify({ test: "success" })],
                                            {type: "application/json"}
                                        ),
                                        {
                                            status: 200,
                                            statusText: "OK",
                                            headers: {
                                            'ETag': 'TestingETagHeader'
                                            },
                                            url: url
                                        }
                                    )
                                )
                            })
    
    init = {
        name: 'basic-integrity',
        uses: [
            {
                name: 'resolve-all',
                description: 'Resolves all',
                version: '0.0.1',
                fetch: resolvingFetch
            }
        ],
        integrity: {
            "https://resilient.is/test.json": "sha384-kn5dhxz4RpBmx7xC7Dmq2N43PclV9U/niyh+4Km7oz5W0FaWdz3Op+3K0Qxz8y3z"
        },
        requireIntegrity: true
    }
    self.log = function(component, ...items) {
        console.debug(component + ' :: ', ...items)
    }
  })
  
  test("it should register in LibResilientPluginConstructors", () => {
    require("../../plugins/basic-integrity.js");
    expect(LibResilientPluginConstructors.get('basic-integrity')(LR, init).name).toEqual('basic-integrity');
  });
  
  test("it should throw an error when there aren't any wrapped plugins configured", async () => {
    require("../../plugins/basic-integrity.js");
    init = {
        name: 'basic-integrity',
        uses: []
    }
    
    expect.assertions(2);
    try {
      await LibResilientPluginConstructors.get('basic-integrity')(LR, init).fetch('https://resilient.is/test.json')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
      expect(e.toString()).toMatch('Expected exactly one plugin to wrap')
    }
  });
  
  test("it should throw an error when there are more than one wrapped plugins configured", async () => {
    require("../../plugins/basic-integrity.js");
    init = {
        name: 'basic-integrity',
        uses: [{
            name: 'plugin-1'
        },{
            name: 'plugin-2'
        }]
    }
    
    expect.assertions(2);
    try {
      await LibResilientPluginConstructors.get('basic-integrity')(LR, init).fetch('https://resilient.is/test.json')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
      expect(e.toString()).toMatch('Expected exactly one plugin to wrap')
    }
  });
  
  
  test("it should return data from the wrapped plugin", async () => {
    require("../../plugins/basic-integrity.js");
    
    const response = await LibResilientPluginConstructors.get('basic-integrity')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(resolvingFetch).toHaveBeenCalled();
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  });
  
  
  test("it should provide the wrapped plugin with integrity data for a configured URL", async () => {
    require("../../plugins/basic-integrity.js");
    
    const response = await LibResilientPluginConstructors.get('basic-integrity')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(resolvingFetch).toHaveBeenCalledWith(
        'https://resilient.is/test.json',
        {
            integrity: init.integrity['https://resilient.is/test.json']
        });
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  });
  
  test("it should error out for an URL with no integrity data, when requireIntegrity is true", async () => {
    require("../../plugins/basic-integrity.js");
    
    expect.assertions(3)
    try {
        const response = await LibResilientPluginConstructors.get('basic-integrity')(LR, init).fetch('https://resilient.is/test2.json');
    } catch (e) {
        expect(e).toBeInstanceOf(Error)
        expect(e.toString()).toMatch('Integrity data required but not provided for')
    }
    expect(resolvingFetch).not.toHaveBeenCalled()
  });
  
  test("it should return data from the wrapped plugin with no integrity data if requireIntegrity is false", async () => {
    require("../../plugins/basic-integrity.js");
    
    init.integrity = {}
    init.requireIntegrity = false
    
    const response = await LibResilientPluginConstructors.get('basic-integrity')(LR, init).fetch('https://resilient.is/test.json');
    
    expect(resolvingFetch).toHaveBeenCalled();
    expect(resolvingFetch).toHaveBeenCalledWith('https://resilient.is/test.json', {});
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  });
  
  test("it should return data from the wrapped plugin with no integrity data configured when requireIntegrity is true and integrity data is provided in Request() init data", async () => {
    require("../../plugins/basic-integrity.js");
    
    init.integrity = {}
    
    const response = await LibResilientPluginConstructors
                            .get('basic-integrity')(LR, init)
                            .fetch('https://resilient.is/test.json', {
                                integrity: "sha256-Aj9x0DWq9GUL1L8HibLCMa8YLKnV7IYAfpYurqrFwiQ="
                            });
    
    expect(resolvingFetch).toHaveBeenCalled();
    expect(resolvingFetch).toHaveBeenCalledWith('https://resilient.is/test.json', {integrity: "sha256-Aj9x0DWq9GUL1L8HibLCMa8YLKnV7IYAfpYurqrFwiQ="});
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  });
  
  test("it should return data from the wrapped plugin with integrity data both configured and coming from Request() init", async () => {
    require("../../plugins/basic-integrity.js");
    
    const response = await LibResilientPluginConstructors
                            .get('basic-integrity')(LR, init)
                            .fetch('https://resilient.is/test.json', {
                                integrity: "sha256-Aj9x0DWq9GUL1L8HibLCMa8YLKnV7IYAfpYurqrFwiQ="
                            });
    
    expect(resolvingFetch).toHaveBeenCalled();
    expect(resolvingFetch).toHaveBeenCalledWith('https://resilient.is/test.json', {integrity: "sha256-Aj9x0DWq9GUL1L8HibLCMa8YLKnV7IYAfpYurqrFwiQ= sha384-kn5dhxz4RpBmx7xC7Dmq2N43PclV9U/niyh+4Km7oz5W0FaWdz3Op+3K0Qxz8y3z"});
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  });
  
});
