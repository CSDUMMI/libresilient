const makeServiceWorkerEnv = require('service-worker-mock');

describe("plugin: ipns-ipfs", () => {
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    global.LibResilientPluginConstructors = new Map()
    init = {
        name: 'ipns-ipfs',
        ipnsPubkey: 'stub'
    }
    LR = {
        log: (component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        }
    }
  })
  test("it should register in LibResilientPlugins", () => {
    require("../../plugins/ipns-ipfs.js");
    expect(LibResilientPluginConstructors.get('ipns-ipfs')(LR, init).name).toEqual('ipns-ipfs');
  });
});
