/**
 * @jest-environment jsdom
 */

const makeServiceWorkerEnv = require('service-worker-mock');

global.fetch = require('node-fetch');
jest.mock('node-fetch')

global.fetch.mockImplementation((url, init) => {
          const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: url
                            });
          return Promise.resolve(response);
        });

describe("plugin: cache", () => {
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    global.LibResilientPluginConstructors = new Map()
    LR = {
        log: (component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        }
    }
  })
  
  test("it should register in LibResilientPluginConstructors", () => {
    require("../../plugins/cache.js");
    expect(LibResilientPluginConstructors.get('cache')().name).toEqual('cache');
  });
  
  test("it should error out if resource is not found", () => {
    require("../../plugins/cache.js");
    
    expect.assertions(1)
    return expect(LibResilientPluginConstructors.get('cache')(LR).fetch('https://resilient.is/test.json')).rejects.toThrow(Error)
  });
  
  test("it should stash a url successfully", () => {
    require("../../plugins/cache.js");
    expect.assertions(7);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash('https://resilient.is/test.json').then((result)=>{
      expect(result).toEqual(undefined)
      return cachePlugin.fetch('https://resilient.is/test.json')
    }).then(fetchResult => {
      expect(fetchResult.status).toEqual(200)
      expect(fetchResult.statusText).toEqual('OK')
      expect(fetchResult.url).toEqual('https://resilient.is/test.json')
      expect(fetchResult.headers.has('Etag')).toEqual(true)
      expect(fetchResult.headers.get('ETag')).toEqual('TestingETagHeader')
      return fetchResult.json().then(json => {
        expect(json).toEqual({ test: "success" })
      })
    })
  });
  
  test("it should clear a url successfully", () => {
    require("../../plugins/cache.js");
    expect.assertions(3);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash('https://resilient.is/test.json').then((result)=>{
      expect(result).toBe(undefined)
      return cachePlugin.unstash('https://resilient.is/test.json')
    }).then(result => {
      expect(result).toEqual(true)
      return expect(cachePlugin.fetch('https://resilient.is/test.json')).rejects.toThrow(Error)
    })
  });
  
  test("it should stash an array of urls successfully", () => {
    require("../../plugins/cache.js");
    expect.assertions(13);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash(['https://resilient.is/test.json', 'https://resilient.is/test2.json']).then((result)=>{
      expect(result).toEqual([undefined, undefined])
      return cachePlugin.fetch('https://resilient.is/test.json')
    }).then(fetchResult => {
      expect(fetchResult.status).toEqual(200)
      expect(fetchResult.statusText).toEqual('OK')
      expect(fetchResult.url).toEqual('https://resilient.is/test.json')
      expect(fetchResult.headers.has('Etag')).toEqual(true)
      expect(fetchResult.headers.get('ETag')).toEqual('TestingETagHeader')
      return fetchResult.json().then(json => {
        expect(json).toEqual({ test: "success" })
      })
    }).then(() => {
      return cachePlugin.fetch('https://resilient.is/test2.json')
    }).then(fetchResult => {
      expect(fetchResult.status).toEqual(200)
      expect(fetchResult.statusText).toEqual('OK')
      expect(fetchResult.url).toEqual('https://resilient.is/test2.json')
      expect(fetchResult.headers.has('Etag')).toEqual(true)
      expect(fetchResult.headers.get('ETag')).toEqual('TestingETagHeader')
      return fetchResult.json().then(json => {
        expect(json).toEqual({ test: "success" })
      })
    })
  });
  
  test("it should clear an array of urls successfully", () => {
    require("../../plugins/cache.js");
    expect.assertions(4);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash(['https://resilient.is/test.json', 'https://resilient.is/test2.json']).then((result)=>{
      expect(result).toEqual([undefined, undefined])
      return cachePlugin.unstash(['https://resilient.is/test.json', 'https://resilient.is/test2.json'])
    }).then(result => {
      expect(result).toEqual([true, true])
      return expect(cachePlugin.fetch('https://resilient.is/test.json')).rejects.toThrow(Error)
    }).then(()=>{
      return expect(cachePlugin.fetch('https://resilient.is/test2.json')).rejects.toThrow(Error)
    })
  });
  
  
  test("it should error out when stashing a Response without a url/key", () => {
    require("../../plugins/cache.js");
    
    const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              }
                            });
    response.url=''
    
    expect.assertions(1);
    return expect(LibResilientPluginConstructors.get('cache')(LR).stash(response)).rejects.toThrow(Error)
  });
  
  test("it should stash a Response successfully", () => {
    require("../../plugins/cache.js");
    
    const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: 'https://resilient.is/test.json'
                            });
    
    expect.assertions(7);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash(response).then((result)=>{
      expect(result).toEqual(undefined)
      return cachePlugin.fetch('https://resilient.is/test.json')
    }).then(fetchResult => {
      expect(fetchResult.status).toEqual(200)
      expect(fetchResult.statusText).toEqual('OK')
      expect(fetchResult.url).toEqual('https://resilient.is/test.json')
      expect(fetchResult.headers.has('Etag')).toEqual(true)
      expect(fetchResult.headers.get('ETag')).toEqual('TestingETagHeader')
      return fetchResult.json().then(json => {
        expect(json).toEqual({ test: "success" })
      })
    })
  });
  
  test("it should stash a Response with an explicit key successfully", () => {
    require("../../plugins/cache.js");
    
    const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: 'https://resilient.is/test.json'
                            });
    
    expect.assertions(7);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash(response, 'special-key').then((result)=>{
      expect(result).toEqual(undefined)
      return cachePlugin.fetch('special-key')
    }).then(fetchResult => {
      expect(fetchResult.status).toEqual(200)
      expect(fetchResult.statusText).toEqual('OK')
      expect(fetchResult.url).toEqual('https://resilient.is/test.json')
      expect(fetchResult.headers.has('Etag')).toEqual(true)
      expect(fetchResult.headers.get('ETag')).toEqual('TestingETagHeader')
      return fetchResult.json().then(json => {
        expect(json).toEqual({ test: "success" })
      })
    })
  });
  
  test("it should stash a Response with no url set but with an explicit key successfully", () => {
    require("../../plugins/cache.js");
    
    const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                            });
    response.url = ''
    
    expect.assertions(6);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash(response, 'special-key').then((result)=>{
      expect(result).toEqual(undefined)
      return cachePlugin.fetch('special-key')
    }).then(fetchResult => {
      expect(fetchResult.status).toEqual(200)
      expect(fetchResult.statusText).toEqual('OK')
      expect(fetchResult.headers.has('Etag')).toEqual(true)
      expect(fetchResult.headers.get('ETag')).toEqual('TestingETagHeader')
      return fetchResult.json().then(json => {
        expect(json).toEqual({ test: "success" })
      })
    })
  });
  
  test("it should clear a Response successfully", () => {
    require("../../plugins/cache.js");
    
    const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: 'https://resilient.is/test.json'
                            });
    
    expect.assertions(3);
    var cachePlugin = LibResilientPluginConstructors.get('cache')(LR)
    return cachePlugin.stash(response).then((result)=>{
      expect(result).toBe(undefined)
      return cachePlugin.unstash(response)
    }).then(result => {
      expect(result).toEqual(true)
      return expect(cachePlugin.fetch('https://resilient.is/test.json')).rejects.toThrow(Error)
    })
  });
  
});
